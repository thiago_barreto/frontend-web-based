export class Person {
    public id: number;
    public nome: string;
    public cpf: string;
    public sexo_id: number;
    public dataNascimento: string;
    public endereco: string;
}
