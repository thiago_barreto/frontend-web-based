
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from './../../environments/environment';

@Injectable()
export class HttpService {
    constructor(
        private http: Http
    ) { }

    getHeaders() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return options;
    }

    getApi(): string {
        return environment.HOST_API;
    }
}