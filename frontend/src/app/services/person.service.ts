
import { HttpService } from './http.service';
import 'rxjs/add/operator/map';
import { Person } from '../model/person.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PersonService {
    private api: string;
    constructor(private http: Http, private httpService: HttpService) {
        this.api = httpService.getApi() + 'pessoa/';
    }

    getAll() {
        return this.http.get(this.api)
            .map(res => {
                return res.json();
            });
    }

    get(id: number) {
        if (id && id > 0) {
            return this.http.get(this.api + id)
                .map(res => {
                    return res.json();
                });
        }
    }

    create(person: Person) {
        const header = this.httpService.getHeaders();
        return this.http.post(this.api, person, header)
            .map(res => {
                return res;
            });
    }

    update(id: number, person: Person) {
        if (id && id > 0) {
            person.id = id;
            const header = this.httpService.getHeaders();
            return this.http.put(this.api, person, header)
                .map(res => {
                    return res;
                });
        }
    }

    delete(id: number) {
        return this.http.delete(this.api + id)
            .map(res => {
                return res;
            });
    }
}
