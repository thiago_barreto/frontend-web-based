import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing } from './app.routes';


import { AppComponent } from './app.component';
import { PersonService } from './services/person.service';
import { HttpModule } from '@angular/http';
import { HttpService } from './services/http.service';
import { PersonComponent } from './components/person/person.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { GenderSubstr } from './util/pipes/gender.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CreatePersonComponent } from './components/create-person/create-person.component';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { EditPersonComponent } from './components/edit-person/edit-person.component';
import { RemovePersonComponent } from './components/remove-person/remove-person.component';
import { GraphPersonComponent } from './components/graph-person/graph-person.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonComponent,
    GenderSubstr,
    CreatePersonComponent,
    EditPersonComponent,
    RemovePersonComponent,
    GraphPersonComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [
    HttpService,
    PersonService,
    FormBuilder
  ],
  entryComponents: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
