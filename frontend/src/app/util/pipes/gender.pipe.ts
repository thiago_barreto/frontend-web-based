import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'genderSubstr'})
export class GenderSubstr implements PipeTransform {
  transform(gender: number): string {
    return gender === 1 ? "Masculino" : "Feminino";
  }
}