import { Component, OnInit } from '@angular/core';
import { Person } from '../../model/person.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonService } from '../../services/person.service';
import { AppUtils } from '../../util/app.util';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-remove-person',
  templateUrl: './remove-person.component.html',
  styleUrls: ['./remove-person.component.scss']
})
export class RemovePersonComponent implements OnInit {

  public person: Person;
  id: number;
  error: boolean;
  errorMsg: string;
  success: boolean;
  successMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: PersonService,
    private toastr: ToastrService
  ) {
    this.successMsg = "Cadastro removido com sucesso.";
    this.errorMsg = "Ocorreu um erro!";
    this.id = route.snapshot.params['id'];
    this.person = new Person();
    this.error = false;
    this.success = false;
  }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.person = res;
      this.person.dataNascimento = AppUtils.getBRDateLocaleFormat(this.person.dataNascimento);
    });
  }

  remove() {
    this.api.delete(this.id).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['person']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  showSuccess(msg: string) {
    this.toastr.success(msg, 'Sucesso!', {
      closeButton: true,
      progressBar: true
    });
  }

  showWarning(msg: string) {
    this.toastr.warning(msg, 'Aviso!', {
      closeButton: true,
      progressBar: true
    });
  }

  showError(msg: string) {
    this.toastr.error(msg, 'Erro!', {
      closeButton: true,
      progressBar: true
    });
  }

}
