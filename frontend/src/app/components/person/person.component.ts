import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../services/person.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  public listPessoa: any;
  public saveResult: any;

  public showLoading: boolean;

  constructor(private api: PersonService) { }

  ngOnInit(self = this) {
    
    this.api.getAll().subscribe(res => {
    this.listPessoa = res;
    });
  }

}
