import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphPersonComponent } from './graph-person.component';

describe('GraphPersonComponent', () => {
  let component: GraphPersonComponent;
  let fixture: ComponentFixture<GraphPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
