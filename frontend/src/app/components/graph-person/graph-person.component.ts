import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../services/person.service';
import { Person } from '../../model/person.model';
import { AppUtils } from '../../util/app.util';

@Component({
  selector: 'app-graph-person',
  templateUrl: './graph-person.component.html',
  styleUrls: ['./graph-person.component.scss']
})

export class GraphPersonComponent implements OnInit {

  public listPessoa: Person[];
  public listIdade: any;
  public listSexo: any[];
  single = [];
  multi = [];

  //Chart
  view: any[] = [700, 400];
  showLegend = true;
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  constructor(private api: PersonService) {
    this.listPessoa = [];
    this.listIdade = [];
    this.listSexo = [];
  }

  ngOnInit() {
    this.api.getAll().subscribe(res => {
      this.listPessoa = res;
      this.getListSexo();
      this.getListIdade();
    });

  }

  getListSexo() {
    this.listSexo[0] = { "name": "Feminino", "value": 0 };
    this.listSexo[1] = { "name": "Masculino", "value": 0 };

    for (let person of this.listPessoa) {
      switch (person.sexo_id) {
        case 2:
          this.listSexo[0].value = this.listSexo[0].value + 1;
          break;
        case 1:
          this.listSexo[1].value = this.listSexo[1].value + 1;
          break;
      }
    }

    this.single[0] = this.listSexo;
    console.log(this.single);
  }

  getListIdade() {
    this.listIdade[0] = { "name": "De 0 a 9", "value": 0 };
    this.listIdade[1] = { "name": "De 10 a 19", "value": 0 };
    this.listIdade[2] = { "name": "De 20 a 29", "value": 0 };
    this.listIdade[3] = { "name": "De 30 a 39", "value": 0 };
    this.listIdade[4] = { "name": "Maior que 40", "value": 0 };
    for (let person of this.listPessoa) {
      let age = this.calcAge(new Date(person.dataNascimento));

      if (age <= 9) {
        this.listIdade[0].value = this.listIdade[0].value + 1;
      } else if (age >= 10 && age <= 19) {
        this.listIdade[1].value = this.listIdade[1].value + 1;
      } else if (age >= 20 && age <= 29) {
        this.listIdade[2].value = this.listIdade[2].value + 1;
      } else if (age >= 30 && age <= 39) {
        this.listIdade[3].value = this.listIdade[3].value + 1;
      } else if (age >= 40) {
        this.listIdade[4].value = this.listIdade[4].value + 1;
      }
    }

    this.single[1] = this.listIdade;
    console.log(this.single);
  }

  calcAge(dataNascimento: any) {
    var timeDiff = Math.abs(Date.now() - dataNascimento);
    return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
  }
}
