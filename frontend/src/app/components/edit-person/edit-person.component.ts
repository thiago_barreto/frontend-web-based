import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../services/person.service';
import { Person } from '../../model/person.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AppUtils } from '../../util/app.util';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.scss']
})
export class EditPersonComponent implements OnInit {

  public person: Person;
  id: number;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: PersonService,
    private toastr: ToastrService
  ) {
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro atualizado com sucesso.";
    this.errorMsg = "Ocorreu um erro!";
    this.id = route.snapshot.params['id'];
    this.person = new Person();
  }

  ngOnInit() {

    this.api.get(this.id).subscribe(res => {
      this.person = res;
      this.person.dataNascimento = AppUtils.getDateLocaleFormat(this.person.dataNascimento);
    });
  }

  update() {
    this.api.update(this.id, this.person).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['person']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  validateForm() {
    if (
      !this.person.nome
      || !this.person.cpf 
      || !this.person.dataNascimento 
      || !this.person.sexo_id
      || !this.person.endereco) {
      this.showWarning(this.formErrorMsg);
    } else {
      this.update();
    }

  }

  showSuccess(msg: string) {
    this.toastr.success(msg, 'Sucesso!', {
      closeButton: true,
      progressBar: true
    });
  }

  showWarning(msg: string) {
    this.toastr.warning(msg, 'Aviso!', {
      closeButton: true,
      progressBar: true
    });
  }

  showError(msg: string) {
    this.toastr.error(msg, 'Erro!', {
      closeButton: true,
      progressBar: true
    });
  }

}
