import { Component, OnInit } from '@angular/core';
import { Person } from '../../model/person.model';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { PersonService } from '../../services/person.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.scss']
})
export class CreatePersonComponent implements OnInit  {

  public person: Person;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;

  constructor(
    private api: PersonService,
    private router: Router,
    private toastr: ToastrService,
  ) {
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Pessoa cadastrada com sucesso.";
    this.errorMsg = "Ocorreu um erro!";
    this.person = new Person();
  }

  ngOnInit() {
  }

  save() {
    this.api.create(this.person).subscribe(res => {
      if (res.status === 200) {
        this.cleanForm();
        this.showSuccess(this.successMsg);
        this.router.navigate(['person']);
      } else {
        this.showError(this.errorMsg);
      }
    })
  }

  cleanForm() {
    this.person.nome = "";
    this.person.dataNascimento = "";
    this.person.sexo_id = 0;
    this.person.endereco = "";
    this.person.cpf = "";
  }

  validateForm() {
    if (
      !this.person.nome
      || !this.person.cpf
      || !this.person.dataNascimento
      || !this.person.sexo_id
      || !this.person.endereco) {
      this.showWarning(this.formErrorMsg);
    } else {
      this.save();
    }
  }

  showSuccess(msg: string) {
    this.toastr.success(msg, 'Sucesso!', {
      closeButton: true,
      progressBar: true
    });
  }

  showWarning(msg: string) {
    this.toastr.warning(msg, 'Aviso!', {
      closeButton: true,
      progressBar: true
    });
  }

  showError(msg: string) {
    this.toastr.error(msg, 'Erro!', {
      closeButton: true,
      progressBar: true
    });
  }

}
