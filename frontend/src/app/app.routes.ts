import { RouterModule, Routes } from '@angular/router';
import { PersonComponent } from './components/person/person.component';
import { ModuleWithProviders } from '@angular/core';
import { CreatePersonComponent } from './components/create-person/create-person.component';
import { EditPersonComponent } from './components/edit-person/edit-person.component';
import { RemovePersonComponent } from './components/remove-person/remove-person.component';
import { GraphPersonComponent } from './components/graph-person/graph-person.component';

const appRoutes: Routes = [
    { path: '', component: PersonComponent },
    { path: 'person', component: PersonComponent },
    { path: 'person/create', component: CreatePersonComponent },
    { path: 'person/edit/:id', component: EditPersonComponent },
    { path: 'person/remove/:id', component: RemovePersonComponent },
    { path: 'person/graph', component: GraphPersonComponent },
    { path: '**', component: PersonComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);