const IP_API = 'http://localhost';
const PORT_API = '1080';


export const environment = {
  production: false,
  HOST_API: IP_API + ':' + PORT_API + '/'
};
