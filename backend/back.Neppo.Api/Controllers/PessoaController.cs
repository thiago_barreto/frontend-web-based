﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using back.Neppo.App.Services;
using back.Neppo.App.viewModel;
using back.Neppo.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace back.Neppo.Api.Controllers
{

    public class PessoaController : Controller
    {

        [HttpGet]
        [Produces(typeof(Pessoa))]
        [Route("/pessoa/{id}")]
        public IActionResult Get(int id, [FromServices] PessoaApp pessoaApp)
        {
            try
            {
                return Ok(pessoaApp.GetById(id));
            }
            catch
            {
                return NotFound(id);
            }
        }

        [HttpGet]
        [Produces(typeof(List<Pessoa>))]
        [Route("/pessoa")]
        public List<Pessoa> Get([FromQuery]string nome, [FromServices] PessoaApp pessoaApp)
        {
            try
            {
                return pessoaApp.GetAll(nome);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        [Route("pessoa")]
        public IActionResult Post([FromBody]PessoaViewModel body, [FromServices] PessoaApp pessoaApp)
        {
            try
            {
                pessoaApp.SavePessoa(body);
                return Ok(HttpStatusCode.OK);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Route("pessoa")]
        public IActionResult Put([FromBody]PessoaViewModel body, [FromServices] PessoaApp pessoaApp)
        {
            try
            {
                pessoaApp.EditPessoa(body);
                return Ok(HttpStatusCode.OK);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("pessoa/{id}")]
        public IActionResult Delete(int id, [FromServices] PessoaApp pessoaApp)
        {
            if (id > 0)
                pessoaApp.DeletePessoa(id);

            else
                BadRequest();

            return Ok(HttpStatusCode.OK);
        }
    }
}