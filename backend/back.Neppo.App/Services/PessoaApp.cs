﻿using back.Neppo.App.viewModel;
using back.Neppo.Domain.Entities;
using back.Neppo.Domain.IRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace back.Neppo.App.Services
{
    public class PessoaApp
    {
        private IRepository<Pessoa> _rep;


        public PessoaApp(IRepository<Pessoa> rep)
        {
            _rep = rep;
        }


        public virtual PessoaViewModel GetById(int id)
        {
            try
            {
                var entity = _rep.Get(id);

                PessoaViewModel person = new PessoaViewModel
                {
                    Id = entity.Id,
                    Nome = entity.Nome,
                    Cpf = entity.Cpf,
                    DataNascimento = entity.DataNascimento,
                    Endereco = entity.Endereco,
                    SexoId = entity.SexoId

                };
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual Pessoa GetByNome(string nome)
        {
            return _rep.GetAll(null).Where(x => x.Nome == nome).FirstOrDefault();
        }

        public virtual Pessoa SavePessoa(PessoaViewModel modelView)
        {
            try
            {

                Pessoa entity = new Pessoa
                {
                    Nome = modelView.Nome,
                    Cpf = modelView.Cpf,
                    DataNascimento = modelView.DataNascimento,
                    Endereco = modelView.Endereco,
                    SexoId = modelView.SexoId
                };
                _rep.Insert(entity);

                return entity;
            }
            catch
            {
                throw new ValidationException();
            }

        }


        public virtual Pessoa EditPessoa(PessoaViewModel view)
        {
            try
            {
                //PessoaViewModel view = new PessoaViewModel();
                Pessoa pessoa = _rep.Get(view.Id.Value);

                if (pessoa != null)
                {
                    pessoa.Nome = view.Nome;
                    pessoa.SexoId = view.SexoId;
                    pessoa.DataNascimento = view.DataNascimento;
                    pessoa.Cpf = view.Cpf;
                    pessoa.Endereco = view.Endereco;
                    _rep.Update(pessoa);
                }

                return pessoa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeletePessoa(int id)
        {
            Pessoa pessoa = _rep.Get(id);
            if (pessoa != null)
            {
                _rep.Delete(pessoa);
            }
        }


        public List<Pessoa> GetAll(string nome)
        {
            return _rep.GetAll(null).Where(x => (string.IsNullOrWhiteSpace(nome) || x.Nome.ToUpper().Contains(nome.ToUpper()))).ToList();

        }
    }
}
