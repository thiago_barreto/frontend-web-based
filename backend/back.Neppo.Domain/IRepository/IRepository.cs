﻿using back.Neppo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace back.Neppo.Domain.IRepository
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll(string include);
        T Get(int id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
