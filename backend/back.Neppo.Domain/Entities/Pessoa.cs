﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace back.Neppo.Domain.Entities
{
    [DataContract]
    public class Pessoa : BaseEntity
    {
        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "dataNascimento")]
        public DateTime DataNascimento { get; set; }

        [DataMember(Name = "cpf")]
        public string Cpf { get; set; }

        [DataMember(Name = "endereco")]
        public string Endereco { get; set; }

        [DataMember(Name = "sexo_id")]
        public int SexoId { get; set; }

        [DataMember(Name = "sexo")]

        public Sexo Sexo { get; set; }

    }
}
