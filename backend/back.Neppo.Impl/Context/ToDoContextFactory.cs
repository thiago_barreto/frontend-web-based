﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace back.Neppo.Impl.Context
{
    public class ToDoContextFactory : IDesignTimeDbContextFactory<DataContext>
    {

        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseSqlServer("Data Source=DESKTOP-IM5N05U\\SQLEXPRESS;Initial Catalog=TesteDb; Trusted_Connection = True;");
            return new DataContext(builder.Options);
        }
    }
}
