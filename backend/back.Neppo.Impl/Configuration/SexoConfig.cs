﻿using back.Neppo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace back.Neppo.Impl.Configuration
{
    public class SexoConfig : IEntityTypeConfiguration<Sexo>
    {
        public void Configure(EntityTypeBuilder<Sexo> entity)
        {
            entity.ToTable("SEXO");

            entity.HasKey(e => e.Id);
            entity.Property(e => e.Id).HasColumnName("ID").UseSqlServerIdentityColumn().IsRequired();
            entity.Property(e => e.Nome).HasColumnName("NOME").HasMaxLength(100).IsRequired();

        }
    }
}
