﻿using back.Neppo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace back.Neppo.Impl.Configuration
{
    public class PessoaConfig : IEntityTypeConfiguration<Pessoa>
    {
        public void Configure(EntityTypeBuilder<Pessoa> entity)
        {
            entity.ToTable("PESSOA");

            entity.HasKey(p => p.Id);
            entity.Property(p => p.Id).HasColumnName("ID").UseSqlServerIdentityColumn().IsRequired();
            entity.Property(p => p.Nome).HasColumnName("NOME").HasMaxLength(100).IsRequired();
            entity.Property(p => p.Cpf).HasColumnName("CPF").HasMaxLength(11).IsRequired();
            entity.Property(p => p.Endereco).HasColumnName("Endereco");
            entity.Property(p => p.DataNascimento).IsRequired();
            entity.Property(p => p.SexoId).IsRequired();


            entity.HasOne<Sexo>(p => p.Sexo).WithMany()
                .HasForeignKey(p => p.SexoId).OnDelete(DeleteBehavior.Restrict);

        }
    }
}
